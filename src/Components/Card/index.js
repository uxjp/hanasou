import React from 'react';
import { View } from 'react-native';

import { Container } from './styles';

function Card({ style, children }) {
  return <View style={{ ...Container.root, ...style }}>
      {children}
  </View>;
}

export default Card;