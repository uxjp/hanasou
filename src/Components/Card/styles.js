import { StyleSheet } from 'react-native';

export const Container = StyleSheet.create({
    root: {
        display: 'flex',
        borderWidth: 3,
        borderColor: '#030E1F',
        borderRadius: 10,
        padding: 15,
    }
});
