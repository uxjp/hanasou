import { StyleSheet } from 'react-native';

export const Container = StyleSheet.create({
    root: {
    },
    text: {
        fontSize: 42,
        lineHeight: 42
    }
});
