import React from 'react';
import { Text, View } from 'react-native';

import { Container } from './styles';

function Title({ children, style }) {
      <Text style={{ ...Container.text, ...style }}>
        {children}
      </Text>;
}

export default Title;