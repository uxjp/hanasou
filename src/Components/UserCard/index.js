import React from "react";

import { View, Text } from "react-native";

import Card from "../Card";

import { Container } from "./styles";

function UserCard({ name, languages, city }) {
  return (
    <Card>
      <View>
        <Text style={Container.name}>João Luiz</Text>
        <Text style={Container.basicInfos}>Francês, Inglês</Text>
        <Text style={Container.basicInfos}>Fortaleza</Text>
      </View>
    </Card>
  );
}

export default UserCard;
